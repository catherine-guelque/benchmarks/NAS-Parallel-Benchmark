#!/bin/bash

prefix=$(dirname $(realpath $0))
cd $prefix

mkdir  logs 2>/dev/null

NITER=1

NB_MPI_RANKS=4


for iter in $(seq $NITER) ; do
    for app in $(ls bin); do
	date=$(date +%F_%R:%S)
	logfile=logs/npb_${app}_${NB_MPI_RANKS}_iter_${iter}_${date}.log
	mpirun -np ${NB_MPI_RANKS} ./bin/$app | tee "$logfile"
    done       
done
