#!/bin/bash

prefix=$(dirname $(realpath $0))
cd $prefix

rm -rf bin 2>/dev/null
mkdir bin

make suite
